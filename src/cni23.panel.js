(function ($) {

    $.fn.iPanel = function (options) {
        var defaults = {
            title: '',
            closable: true,
            iconCls: '',
            content: '',
            border: false,
            fit: true
        };

        var options = $.extend(defaults, options);
        var iframe = '<iframe src="' + options.href + '" scrolling="auto" frameborder="0" style="width:100%;height:100%;"></iframe>';

        $(this).panel({
            content: iframe,
            onLoad:function(){
                alert('loaded successfully');
            }
        });
    }

})(jQuery);