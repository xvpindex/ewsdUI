module.exports = function(grunt) {
  //配置参数
  grunt.initConfig({
     pkg: grunt.file.readJSON('package.json'),
     concat: {
         options: {
             separator: ';',
             stripBanners: true
         },
         dist: {
             src: [
                 "src/cni23.combotree.js",
                 "src/cni23.common.js",
                 "src/cni23.core.js",
                 "src/cni23.datagrid2.js",
                 "src/cni23.dialog.js",
                 "src/cni23.dialog2.js",
                 "src/cni23.edatagrid.js",
                 "src/cni23.extend.js",
                 "src/cni23.form.js",
                 "src/cni23.function.js",
                 "src/cni23.menu.js",
                 "src/cni23.panel.js",
                 "src/cni23.plugins.js",
                 "src/cni23.tabs.js",
                 "src/cni23.toolbar.js",
                 "src/cni23.tree.js",
                 "src/cni23.treegrid.js",
                 "src/cni23.window.js"
             ],
             //dest: 'assets/js/cni23.js'
             dest: 'E:/wwwroot/Java/ewsdERP/src/main/webapp/static/cni23/core/cni23.js'
         }
     },
     uglify: {
         options: {
            banner: '/* <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
         },
         dist: {
             files: {
                 //'assets/js/cni23.min.js': 'assets/js/cni23.js',
                 //'E:/wwwroot/Java/ewsdERP/src/main/webapp/static/cni23/core/cni23.min.js': 'assets/js/cni23.js'
                 'E:/wwwroot/Java/ewsdERP/src/main/webapp/static/cni23/core/cni23.min.js': 'E:/wwwroot/Java/ewsdERP/src/main/webapp/static/cni23/core/cni23.js'
             }
         }
     },
     cssmin: {
         options: {
             keepSpecialComments: 0 /* 删除所有注释 */
         },
         compress: {
             files: {
                 'E:/wwwroot/Java/ewsdERP/src/main/webapp/static/cni23/css/style.css': [
                     "themes/css/icon.css",
                     "themes/css/style.css",
                     "themes/css/bootstrap-ext.css"
                 ]
             }
         }
     }
  });

  //载入concat和uglify插件，分别对于合并和压缩
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  //注册任务
  grunt.registerTask('default', ['concat', 'uglify', 'cssmin']);
}