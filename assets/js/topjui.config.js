
var topJUI = {
    language: {
        message : {
            title : {
                operationTips: "操作提示",
                confirmTips: "确认提示"
            },
            msg : {
                success : "操作成功",
                failed : "操作失败",
                error : "未知错误",
                checkSelfGrid : "请先勾选中要操作的数据前的复选框",
                selectSelfGrid : "请先选中要操作的数据",
                selectParentGrid : "请先选中主表中要操作的一条数据",
                permissionDenied : "对不起，你没有操作权限",
                confirmDelete : "你确定要删除所选的数据吗？"
            },
            icon : {
                error : "error",
                question : "question",
                info : "info",
                warning : "warning"
            }
        }
    }
}

